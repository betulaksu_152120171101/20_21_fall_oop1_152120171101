#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
#include <stack>
#include <sstream>
#include <string>
//#include <unordered_map>

using namespace std;


vector<string> tagStack;
map<string, string> ozellik;
    

void ozellikEkle(string & name, string & deger) {
    string all;
    for(string & str : tagStack)
        all += str + ".";
    all.pop_back();
    all += "~" + name;
    ozellik[all] = deger;
    
}

int main() {
    int a,b;
    int i,j;
    cin >> a;
    cin >> b;
    
    for(i = 0; i < a; ++i) {
        char c; 
        cin >> c;
        if(cin.peek() == '/') { 
            string cn; 
            cin >> cn;
            tagStack.pop_back();
        }
        else { 
            string name;
            cin >> name;
            if(name.back() == '>') 
            { 
                name.pop_back();
                tagStack.push_back(name);
            }
            else {
                tagStack.push_back(name);
                 
                for(;;)
                 { 
                    string attrName, attrDeg, eq;
                    cin >> attrName >> eq >> attrDeg;
                    if(attrDeg.back() == '>')
                     { 
                        attrDeg.pop_back();
                        attrDeg.pop_back();
                        attrDeg = attrDeg.substr(1);
                        ozellikEkle(attrName, attrDeg);
                        
                        break;
                    }
                    else 
                    {
                        attrDeg.pop_back();
                        attrDeg = attrDeg.substr(1);
                        ozellikEkle(attrName, attrDeg);
                    }
                }
           
            }
                
       
       
        }
    }
    
    for(i = 0; i < b; ++i) {
        string sor;
        cin>>sor;
        if(ozellik.find(sor) != ozellik.end())
            cout <<ozellik[sor]<<endl;
        else 
            cout <<"Not Found!"<<endl;
    }
    

    return 0;
}
