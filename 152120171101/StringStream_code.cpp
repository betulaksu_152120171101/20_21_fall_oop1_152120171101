#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    
    stringstream ss(str);
    
    vector<int> sonuc;
    char charr;
    int sayac;

    while (ss >> sayac) {
        sonuc.push_back(sayac);
        ss >> charr;
    }

    return sonuc;
}

int main() 
{
    int i;
    string str;
    cin >> str;
    
    vector<int> value = parseInts(str);

    for (i = 0; i < value.size(); i++) {
        cout<<value[i] ;
        cout<< endl;
    }

    return 0;
}
