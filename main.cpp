#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

/**  @brief this is the introduction of the program 
*/
int main() {
    /**  Detailed description  the member
    @param sum is the number to add
    @param avg  is the average of the numbers added
    @param smallest is the smallest number
    @param product is the number to product
    @param filename keeps the file name received from the user
    @param i shows the value we want to achieve
    @param num  shows how many numbers
   
*/
    int sum = 0;  
    float avg;
    int smallest;
    int product=1;
    char filename[100];
    int i;
    int num[70];
    
  
    /**
    @brief  getting filename from user
 
*/
    ifstream inFile;  ///object for reading from a file 
   
    cout << "Enter the file name: ";
    cin>>filename;
    
       /**
    @brief  Openes the file descriptor
*/
    inFile.open(filename, ios::in);


     /**
    @brief ///terminate with error
*/
    if (!inFile) {
        cout << "Unable to open file (move your file to the same directory)";
        exit(1); 
    }
    
    inFile>>filename;
    while (inFile >> num[i]) 
	{
        smallest=num[0];
        if(num[i]<smallest){
            smallest=num[i];/* finds the smallest by comparing numbers */
        }
        sum = sum + num[i]; /* adds values in text to sum */
        product = product*num[i]; /* multiplies the numbers in text */
        i++;
        avg=(float)sum/(float)i; /* divides the sum by the number of elements */
    }
    
    inFile.close();
    
    
       /**
    @brief prints sum,product,average and smallest
 
*/
    
    cout << "Sum is = " << sum << endl; 
    cout << "Product is = " << product << endl; 
    cout << "Average is = " << avg << endl; 
    cout << "Smallest is = " << smallest << endl;
    
    return 0;
}
	

